# DESCRIÇÃO #

Este repositório contém código relacionado ao trabalho de comunicação por sockets da disciplina ENG04052 da UFRGS - 1º Semestre de 2014.

Aqui pode ser encontrado:

* Implementação do servidor que será usado na apresentação
* Implementação de exemplo de um cliente em Python
* Implementação de exemplo de um cliente em Java
* Código-fonte (Beamer) dos slides de explicação mostrados em aula (o PDF encontra-se no Moodle)

## Instruções para uso do servidor ##

O servidor é implementado na linguagem Python. Para sistemas Windows, existem pacotes pré-compilados com todas as dependências.

Para outros sistemas, o servidor depende dos seguintes programas/bibliotecas:

* Python (2.7 ou 3.2+)
* numpy
* PySide

O script principal do servidor é `server.py`. Existe ainda uma versão sem interface gráfica do servidor (`server_cli.py`), que necessita *apenas* do interpretador Python instalado.

### Pacotes pré-compilados para Windows ###
Na [seção de downloads](https://bitbucket.org/rafaellerm/servidor-tanque/downloads) existem pacotes pré-compilados para Windows (x86 e amd64). Basta descompatctar o pacote e executar o arquivo `server.exe`

## Implementações-exemplo de clientes ##

Estão ainda inclusos implementações de dois clientes:

* Cliente em linguagem Python: `example_client.py`
* Cliente em linguagem Java: diretório `java_client` (estrutura de diretórios usual do Eclipse)

Por mais que o protocolo seja simples e os exemplos sejam diretos, ainda assim existem outras maneiras de implementar o cliente mesmo nas linguagens acima. E sobre hipótese alguma copie código do exemplo sem saber o quê ele faz ou por quê existe.

### Alterações ###

Atualizações neste repositório são esperadas à medida que forem necessárias para corrigir bugs ou implementar melhorias.