#!/usr/bin/env python3

from PySide import QtGui, QtCore

class StringListWidget(QtGui.QListWidget):
	def __init__(self, parent=None):
		super(StringListWidget, self).__init__(parent)
	@QtCore.Slot(list)
	def set_strings(self, strings):
		self.clear()
		self.addItems(strings)
	widget_closed = QtCore.Signal()
	def closeEvent(self, event):
		self.widget_closed.emit()
		QtGui.QListWidget.closeEvent(self,event)
