#!/usr/bin/env python3

import time
import threading
import socket
import errno
import struct
import logging

# import traceback

def enum(*sequential, **named):
    enums = dict(zip(sequential, range(len(sequential))), **named)
    return type('Enum', (), enums)

def format_hex(x):
    if type(x) != str:
        return ' '.join(['{:02X}'.format(i) for i in x])
    else:
        return ' '.join(['{:02X}'.format(ord(i)) for i in x])

class Tank(object):
    """Represents a liquid Tank, with its dynamics and the network server"""
    log_points = 500
    acceleration_factor = 1
    sock_state_t = enum("WAIT_CMD", "WAIT_ADDR", "WAIT_DATA")
    cmd_t = enum(READ=0, WRITE=1)
    def __init__(self):
        super(Tank, self).__init__()
        self.capacity = 1000
        self.thr_low = 300
        self.thr_high = 800
        self.level = 0
        # Liters/second
        self.in_flow = 200.
        self.out_flow = 50.
        # Seconds between dynamic update
        self.update_interval = 0.1
        self.time = 0.
        self.time_axis = [0 for x in range(self.log_points)]
        self.level_log = [0 for x in range(self.log_points)]
        # Valve state
        self.valve_aperture = 0.
        self.terminated_flag = False
        self.terminated_flag_lock = threading.Lock()
        self.log_lock = threading.Lock()
        self.parameter_lock = threading.Lock()

    def update_dynamic(self):
        while not self.terminated():
            self.next_call += self.update_interval
            self.time += self.update_interval * self.acceleration_factor
            with self.parameter_lock:
                d_level = (self.in_flow*self.valve_aperture) - self.out_flow
                d_level *= self.update_interval
                self.level += d_level
                if self.level < 0:
                    logging.warning(self.peer+" - Empty - "+str(self.level))
                elif self.level > self.capacity:
                    logging.warning(self.peer+" - Overflow")
                self.level = max(self.level, 0.)
            with self.log_lock:
                self.time_axis.pop(0)
                self.time_axis.append(self.time)
                self.level_log.pop(0)
                self.level_log.append(self.level)
            # Termination condition
            # if self.time > 10.:
            #     self.terminated(True)
            # print(self.next_call, self.level)
            time.sleep(self.next_call - time.time())
            # Test
            # if np.floor(self.time/5)%2 == 0:
            #     self.valve_aperture = 1.
            # else:
            #     self.valve_aperture = 0.
        logging.info(self.peer + " - Dynamic thread exiting")

    def server_function(self, *args):
        if args[0] == None:
            print("No socket passed")
            return
        else:
            sock = args[0]
            self.sock = sock
            sock.settimeout(0.5)
            finished = False
            self.state = self.sock_state_t.WAIT_CMD
            self.partial_data = b""
            try:
                while not finished:
                    try:
                        buf = sock.recv(1024)
                        if len(buf) == 0: # Then the socket was closed
                            logging.info(self.peer + " - Connection closed by client")
                            finished = True
                        else:
                            # print(buf.decode(encoding='UTF-8'))
                            # logging.debug(self.peer + " - read from socket: "+format_hex(buf))
                            print(self.peer + " - read from socket: "+format_hex(buf))
                            self.process_cmd(buf)
                    except socket.timeout:
                        pass
                    except socket.error as e:
                        if e.errno != errno.ECONNRESET:
                            raise
                        logging.info(self.peer + "Connection reset by client")
                        finished = True
                    finished = finished or self.terminated()
            finally:
                self.terminated(True)
                sock.close()
            logging.info(self.peer + " - Server thread exiting")

    def process_cmd(self, buff):
        "Process the commands from the socket, using a state machine."
        buff_size = len(buff)
        buff_index = 0
        # print("Buffer", buff)
        while buff_size > 0:
            if self.state == self.sock_state_t.WAIT_CMD:
                self.cmd = struct.unpack("!B", buff[buff_index:buff_index+1])[0]
                buff_index += 1
                buff_size -= 1
                self.state = self.sock_state_t.WAIT_ADDR
            elif self.state == self.sock_state_t.WAIT_ADDR:
                self.cmd_addr = struct.unpack("!B", buff[buff_index:buff_index+1])[0]
                buff_index += 1
                buff_size -= 1
                # print("Command: ",self.cmd, " address: ", self.cmd_addr)
                if self.cmd == self.cmd_t.READ:
                    self.read_register()
                    self.state = self.sock_state_t.WAIT_CMD
                elif self.cmd == self.cmd_t.WRITE:
                    self.state = self.sock_state_t.WAIT_DATA
                else:
                    self.state = self.sock_state_t.WAIT_CMD
            elif self.state == self.sock_state_t.WAIT_DATA:
                payload_size = buff_size if buff_size < 4 else 4
                self.partial_data += buff[buff_index:buff_index+payload_size]
                buff_index += payload_size
                buff_size -= payload_size
                if len(self.partial_data) >= 4:
                    try:
                        new_val = struct.unpack("!I", self.partial_data)[0]
                    except:
                        print('data size:',len(self.partial_data))
                        raise
                    # Write new value
                    self.write_register(new_val)
                    self.partial_data = b''
                    self.state = self.sock_state_t.WAIT_CMD
            else:
                pass

    def read_register(self):
        read_val = int((2**32)-1)
        addr = self.cmd_addr
        if addr == 1:
            with self.parameter_lock:
                read_val = self.level
        elif addr == 2:
            read_val = self.capacity
        elif addr == 3:
            read_val = self.thr_low
        elif addr == 4:
            read_val = self.thr_high
        elif addr == 5:
            with self.parameter_lock:
                read_val = int(self.valve_aperture * (2**32-1))
        else:
            logging.warning(self.peer+" - Tried to read invalid register: "+str(addr))
            addr = 0
        # print("val: ",read_val)
        message = struct.pack("!BBI", self.cmd, addr, int(read_val))
        print(self.peer + " - sent to socket:   "+format_hex(message))
        self.sock.sendall(message)

    def write_register(self, val):
        addr = self.cmd_addr
        read_val = int((2**32)-1)
        if addr == 5:
            # Really, the only writable thing is the valve aperture
            with self.parameter_lock:
                self.valve_aperture = float(val/(2**32 -1))
                read_val = int(self.valve_aperture * (2**32-1))
                # print("Updating valve: ",self.valve_aperture)
        else:
            logging.warning(self.peer+" - Tried to write invalid register: "+str(addr))
            addr = 0
        message = struct.pack("!BBI", self.cmd, addr, read_val)
        print(self.peer + " - sent to socket:   "+format_hex(message))
        self.sock.sendall(message)

    def run(self, socket=None):
        "Start dynamic simulation"
        addr = socket.getpeername()
        self.peer = addr[0] + ":"+str(addr[1])
        logging.info(self.peer+" - Started server")

        self.next_call = time.time()
        self.dynamic_thread = threading.Thread(target=self.update_dynamic)
        self.server_thread = threading.Thread(target=self.server_function, args=(socket,))
        self.dynamic_thread.start()
        self.server_thread.start()

    def stop(self):
        self.terminated(True)
        # self.timer.stop()

    def wait_finished(self):
        self.dynamic_thread.join()
        self.server_thread.join()

    def terminated(self, flag=None):
        with self.terminated_flag_lock:
            if flag != None:
                self.terminated_flag = flag
                # print("asked to terminate")
                # traceback.print_stack()
            return self.terminated_flag

if __name__ == "__main__":
    tank = Tank()
    tank.run()

    tank.wait_finished()
