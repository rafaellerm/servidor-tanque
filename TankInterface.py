#!/usr/bin/env python3

# from PySide import QtGui, QtCore
import PySide
QtGui = PySide.QtGui
QtCore = PySide.QtCore
import pyqtgraph as pg
# import numpy as np

from Tank import Tank

class TankInterface(QtCore.QObject):
    # stop_interface = QtCore.Signal()
    def __init__(self, tank, window_title="Tank Level"):
        super(TankInterface, self).__init__()
        self.tank = tank
        plot_w = pg.plot(title=window_title)
        plot_w.enableAutoRange(False)
        plot_w.setYRange(-self.tank.capacity*0.1, self.tank.capacity*1.1)
        plot_w.addLine(y=tank.capacity, pen='r')
        plot_w.addLine(y=tank.thr_low, pen='g')
        plot_w.addLine(y=tank.thr_high, pen='g')
        self.level_plot = plot_w.plot(pen='y')
        self.plot_w = plot_w
    def update_plot(self):
        t = self.tank
        if t.terminated():
            self.plot_w.win.close()
        else:
            with t.log_lock:
                self.level_plot.setData(t.time_axis, t.level_log)
                self.plot_w.setXRange(t.time_axis[0], t.time_axis[-1])
        return t.terminated()
    @QtCore.Slot()
    def terminate(self):
        self.tank.terminated(True)
        self.tank.wait_finished()
        self.plot_w.win.close()

# def start_tank_gui(sock):
#     app = QtGui.QApplication([])
#     tank = Tank()
#     addr = sock.getpeername()
#     title = "Tank at " + addr[0] + ":"+str(addr[1])
#     tank_viewer = TankInterface(tank, window_title=title)
#     def update_interface():
#         # if np.floor(tank_viewer.tank.time/5)%2 == 0:
#         #     tank_viewer.tank.valve_aperture = 1.
#         # else:
#         #     tank_viewer.tank.valve_aperture = 0.
#         tank_viewer.update_plot()

#     tank.run(sock)
#     timer = QtCore.QTimer()
#     timer.timeout.connect(update_interface)
#     tank_viewer.stop_interface.connect(timer.stop)
#     timer.start(100)

#     import sys
#     if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
#         QtGui.QApplication.instance().exec_()
#         print("Exec ended")
#     tank.stop()
#     timer.stop()
