#!/usr/bin/env python3

import socket
import time
import struct

import argparse
parser = argparse.ArgumentParser()
parser.add_argument("-s", "--server", help="Server address to connect to.",default='')
parser.add_argument("-p", "--port", help="TCP port to coneect on server.", default=5001, type=int)
parser.add_argument("-l", "--loops", help="Number of control loops to execute.",default=100, type=int)
parser.add_argument("-i", "--interval", help="Time to wait between control loops.",default=1, type=float)
args = parser.parse_args()

def format_hex(x):
    if type(x) != str:
        return ' '.join(['{:02X}'.format(i) for i in x])
    else:
        return ' '.join(['{:02X}'.format(ord(i)) for i in x])

s = socket.socket()
s.connect((args.server, args.port))
print("connected")

cmds = args.loops
valve_opened = False
# while cmds > 0:

print("Read level")
request = struct.pack("!BB", 0, 1)
s.sendall(request)
response = s.recv(6)
print("\t" + format_hex(response))
_,_,level = struct.unpack("!BBI", response)
print("\tlevel: "+str(level))

print("Read capacity")
request = struct.pack("!BB", 0, 2)
s.sendall(request)
response = s.recv(6)
print("\t" + format_hex(response))
_,_,cap = struct.unpack("!BBI", response)
print("\tcapacity: " + str(cap))

print("Read thr_low")
request = struct.pack("!BB", 0, 3)
s.sendall(request)
response = s.recv(6)
print("\t" + format_hex(response))
_,_,thr_low = struct.unpack("!BBI", response)
print("\tthr_low: " + str(thr_low))

print("Read thr_high")
request = struct.pack("!BB", 0, 4)
s.sendall(request)
response = s.recv(6)
print("\t" + format_hex(response))
_,_,thr_high = struct.unpack("!BBI", response)
print("\tthr_high: " + str(thr_high))


while cmds > 0:
    print("Write level:")
    if level < thr_low:
        valve_opened = True
    elif level > thr_high:
        valve_opened = False
    aperture = int((2**32)-1) if valve_opened else 0

    request = struct.pack("!BBIBB", 1, 5, aperture, 0, 2)

    s.sendall(request)
    response = s.recv(6)
    print("\tOpened: " + str(valve_opened) )
    print("\t" + format_hex(response))

    response = s.recv(6)
    print("\t" + format_hex(response))
    _,_,cap = struct.unpack("!BBI", response)
    print("\tcapacity: " + str(cap))

    print("Read level")
    request = struct.pack("!BB", 0, 1)
    s.sendall(request)
    response = s.recv(6)
    print("\t" + format_hex(response))
    _,_,level = struct.unpack("!BBI", response)
    print("\tlevel: "+str(level))

    cmds -= 1
    time.sleep(args.interval)

s.close()
