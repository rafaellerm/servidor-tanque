rmdir /S /Q build

py -3.3 setup.py build
py -3.3-32 setup.py build

cd build

move exe.win-amd64-3.3 server-amd64
"%programfiles%\7-zip\7z" a server-amd64.zip server-amd64

move exe.win32-3.3 server-x86
"%programfiles%\7-zip\7z" a server-x86.zip server-x86