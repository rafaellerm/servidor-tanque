package java_client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class TankController {

	/**
	 * The <code>OutputStream</code> for the socket. Works in the same way as
	 * other types of stream output (e.g. files, console).
	 */
	private DataOutputStream outputStream;
	/**
	 * The <code>InputStream</code> for the connected socket.
	 */
	private DataInputStream inputStream;

	private Socket socket;

	// Parameters for the tank
	double maxCapacity;
	double thrHigh;
	double thrLow;

	/**
	 * Constructor for the TankController object.
	 * 
	 * @param serverName
	 *            String representation of the server address -- passed directly
	 *            to the <code>java.net.Socket</code> constructor
	 * @param port
	 *            Port number to connect to
	 * @throws IOException
	 */
	public TankController(final String serverName, final int port) throws IOException {
		socket = new Socket(serverName, port);
		outputStream = new DataOutputStream(socket.getOutputStream());
		inputStream = new DataInputStream(socket.getInputStream());
	}

	/**
	 * Reads the tank parameters one time, then enters the main control loop. In
	 * the loop, it reads the current level, updates the valve, and sleeps for
	 * 500ms.
	 * 
	 * @throws IOException
	 */
	public void runControl() throws IOException {
		try {
			this.getInitialParameters();
			// Loop reading and writing
			while (true) {
				double level = readRemoteRegister(RemoteAddress.LEVEL);
				if (level <= thrLow)
					writeRemoteRegister(RemoteAddress.VALVE_APERTURE, 1.0);
				else if (level >= thrHigh)
					writeRemoteRegister(RemoteAddress.VALVE_APERTURE, 0.0);
				Thread.sleep(500);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			socket.close();
		}
	}

	/**
	 * Reads the tank capacity, and the high and low thresholds.
	 * 
	 * @throws IOException
	 */
	private void getInitialParameters() throws IOException {
		maxCapacity = readRemoteRegister(RemoteAddress.CAPACITY);
		thrHigh = readRemoteRegister(RemoteAddress.THR_HIGH);
		thrLow = readRemoteRegister(RemoteAddress.THR_LOW);
		System.out
				.printf("Maximum capacity: %e\nHigh threshold: %e\nLow threshold: %e\n",
						maxCapacity, thrHigh, thrLow);
	}

	/**
	 * Read a "register" located in the server. Some low-level-ish bit
	 * manipulation is needed because Java has no unsigned types.
	 * 
	 * @param addr
	 *            Address to read from
	 * @return Value read
	 * @throws IOException
	 */
	private double readRemoteRegister(final RemoteAddress addr) throws IOException {
		// Send read command
		outputStream.writeByte(0);
		outputStream.writeByte(addr.getAddress());
		outputStream.flush();

		// Read server response
		int command = inputStream.readUnsignedByte();
		int address = inputStream.readUnsignedByte();
		// Deal with the unsignedness of the response (reading an int and
		// getting only the least-significant 32 bits)
		long response = inputStream.readInt();
		response &= 0xFFFFFFFFL;

		// Error detection code
		if (command != 0 || address != addr.getAddress()) {
			throw new IOException("Server responded with an error code!");
		}

		// Valve aperture will return a value in the interval (0,1)
		if (addr == RemoteAddress.VALVE_APERTURE) {
			return ((double) response) / 0xFFFFFFFFL;
		}
		// System.out.println((double) response);
		return response;
	}

	/**
	 * Write to a "register" located in the server. The double values will be
	 * converted to the appropriate unsigned int encoding. There is only one
	 * actual register that can be written to (the valve aperture). Some
	 * low-level-ish bit manipulation is needed because Java has no unsigned
	 * types.
	 * 
	 * @param addr
	 * @param value
	 * @throws IOException
	 */
	private void writeRemoteRegister(final RemoteAddress addr, final double value)
			throws IOException {
		// Write register
		outputStream.writeByte(1);
		outputStream.writeByte(addr.getAddress());
		// Convert the value to be written an unsigned int representation
		int codedValue = (int) ((long) (value * 0xFFFFFFFFL) & 0xFFFFFFFFL);
		outputStream.writeInt(codedValue);

		// Verify the server response
		int command = inputStream.readUnsignedByte();
		int address = inputStream.readUnsignedByte();
		// Deal with the unsignedness of the response
		long response = inputStream.readInt();
		response &= 0xFFFFFFFFL;

		if (command != 1 ||
				address != addr.getAddress() ||
				(int) response != codedValue) {
			throw new IOException("Server responded with an error code!");
		}
	}

	/**
	 * Provides an safe and abstract way to refer to the addresses of registers.
	 * This way, we do not need to refer to the numeric addresses in the rest of
	 * the code.
	 */
	private enum RemoteAddress {
		ERROR((byte) 0),
		LEVEL((byte) 1),
		CAPACITY((byte) 2),
		THR_LOW((byte) 3),
		THR_HIGH((byte) 4),
		VALVE_APERTURE((byte) 5);

		private byte address;

		private RemoteAddress(final byte addr) {
			this.address = addr;
		}

		/**
		 * Gets the numeric value of the address.
		 * 
		 * @return
		 */
		public byte getAddress() {
			return address;
		}
	}

	/**
	 * Entry point for the application. Simply creates an instance of this class
	 * and runs its loop. Expects two command line arguments:
	 * <ol>
	 * <li>The first one is the server address;</li>
	 * <li>The second one is the port number in the server.</li>
	 * </ol>
	 * 
	 * @throws Exception
	 */
	public static void main(final String[] args) throws Exception {
		String serverName = args[0];
		int port = Integer.parseInt(args[1]);

		TankController client = new TankController(serverName, port);
		client.runControl();
	}

}
