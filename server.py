#!/usr/bin/env python3

from PySide import QtGui, QtCore
# import PySide
# import PySide.QtGui
# QtGui = PySide.QtGui
# import PySide.QtCore
# QtCore = PySide.QtCore

import pyqtgraph as pg
import argparse
import sys
import socket
import logging
from threading import Thread, Lock

from Tank import Tank
from TankInterface import TankInterface
from StringListWidget import StringListWidget

parser = argparse.ArgumentParser()
parser.add_argument("-p", "--port", help="TCP port to listen to", default=5001, type=int)
args = parser.parse_args()

class connection_listener(QtCore.QObject):
    new_connection = QtCore.Signal(socket.socket)
    def __init__(self, parent=None):
        super(connection_listener, self).__init__(parent)
        self.close_flag_lock = Lock()
        self.close_flag = False
    def serve(self, port):
        self.worker_thread = Thread(target=self.listen, args=(port,))
        self.worker_thread.start()
    def listen(self, port):
        listener_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        listener_socket.settimeout(0.5)
        listener_socket.bind(('', port))
        listener_socket.listen(5)
        try:
            while True:
                try:
                    client_socket, _ = listener_socket.accept()
                    addr = client_socket.getpeername()
                    addr = addr[0] + ":"+str(addr[1])
                    logging.info(addr + " - Accepted new connection")
                    self.new_connection.emit(client_socket)
                    logging.debug("New connection signal emitted")
                except socket.timeout:
                    if self.finished():
                        break
        finally:
            listener_socket.close()
            logging.info("Main server exiting")

    def join():
        self.worker_thread.join()

    def finished(self, flag=None):
        with self.close_flag_lock:
            if flag != None:
                self.close_flag = flag
            return self.close_flag
    @QtCore.Slot()
    def terminate(self):
        self.finished(True)

class tank_set(QtCore.QObject):
    clients_changed = QtCore.Signal(list)
    def __init__(self, parent=None):
        super(tank_set, self).__init__(parent)
        self.tanks = []
    @QtCore.Slot(socket.socket)
    def launch_tank(self, sock):
        logging.debug("New connection slot entered")
        title = addr = sock.getpeername()
        title = "Client at " + addr[0] + ":"+str(addr[1])

        tank = Tank()
        ti = TankInterface(tank, window_title=title)
        self.tanks.append(ti)
        app = QtGui.QApplication.instance()
        app.aboutToQuit.connect(ti.terminate)

        tank.run(socket=sock)
        logging.debug("New connection slot exited")
        self.update_client_list()
    @QtCore.Slot()
    def update_plot(self):
        l = len(self.tanks)
        self.tanks[:] = [
            ti for ti in self.tanks
            if not ti.update_plot()
        ]
        if l != len(self.tanks):
            self.update_client_list()
    def update_client_list(self):
        names = [ti.tank.peer for ti in self.tanks]
        self.clients_changed.emit(names)
    @QtCore.Slot()
    def close_all(self):
        for ti in self.tanks:
            ti.terminate()

if __name__ == '__main__':
    logging.basicConfig(filename="server.log", level=logging.DEBUG, format='%(asctime)s %(message)s')
    app = QtGui.QApplication(sys.argv)

    listener = connection_listener()
    tanks = tank_set()

    list_widget = StringListWidget()
    tanks.clients_changed.connect(list_widget.set_strings)
    list_widget.widget_closed.connect(tanks.close_all)
    list_widget.setWindowTitle("Connected clients")
    list_widget.show()

    timer = QtCore.QTimer()
    timer.timeout.connect(tanks.update_plot)
    timer.start(100)

    listener.new_connection.connect(tanks.launch_tank)
    listener.serve(args.port)

    app.aboutToQuit.connect(listener.terminate)
    app.exec_()
    logging.debug("End of main")
