#!/usr/bin/env python3

import socket
from threading import Thread
import logging

from Tank import Tank

import argparse
parser = argparse.ArgumentParser()
# parser.add_argument("-c","--cli", help="run without graphical interface", action="store_true")
parser.add_argument("-p", "--port", help="TCP port to listen to", default=5001, type=int)
args = parser.parse_args()

logging.basicConfig(filename="server.log", level=logging.INFO, format='%(asctime)s %(message)s')

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(('', args.port))
s.listen(5)

logging.info("Server started listening at port "+str(args.port))

def process_cli(s):
    addr = s.getpeername()
    peer = addr[0] + ":"+str(addr[1])
    print(peer + " - Accepted connection")
    tank = Tank()
    tank.run(s)
    tank.wait_finished()
    print(peer + " - Closed connection")

# target = process_cli
# if not args.cli:
#     logging.info("Server started with GUI option")
#     from TankInterface import *
#     def process_gui(s):
#         # s.sendall("Hello World!".encode(encoding='UTF-8'))
#         start_tank_gui(s)
#     target = process_gui

while True:
    try:
        c, _ = s.accept()
        addr = c.getpeername()
        logging.info("Accepted connection to " + addr[0] + ":"+str(addr[1]))
        p = Thread(target=process_cli, args=(c,))
        p.start()
        # c.close()
        # logging.info("started process")
    except KeyboardInterrupt:
        break
print("Server closing")
logging.info("Server closing")
s.close()
