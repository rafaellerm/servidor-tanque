import sys
from cx_Freeze import setup, Executable
import distutils

# distutils.dir_util.remove_tree("build")

# Dependencies are automatically detected, but it might need fine tuning.
build_exe_options = {
	"packages": ["os"], 
	"excludes": ["PyQt4", "scipy", "matplotlib", "QtNetwork4", 
		"multiprocessing", "ssl", "lzma", "bz2", "hashlib", "elementtree", 
		"PySide.QtNetwork",], 
	"includes": ["atexit"],
	"optimize": 2,
	"compressed":True,
	"include_msvcr":True
	}

# GUI applications require a different base on Windows (the default is for a
# console application).
base = None
# if sys.platform == "win32":
#     base = "Win32GUI"

setup(  name = "servidor-tanque",
        version = "0.1",
        description = "...",
        options = {"build_exe": build_exe_options},
        executables = [Executable("server.py", base=base)])